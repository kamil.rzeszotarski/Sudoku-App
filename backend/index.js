const express = require('express')

const app = express()

app.get('/info', (req, res) =>{
    res.send('<p>Info page</p>')
})

app.get('')

const PORT = process.env.PORT || 3001


app.listen(PORT, () => {
    console.log(`Server running on port ${PORT}.`)
})