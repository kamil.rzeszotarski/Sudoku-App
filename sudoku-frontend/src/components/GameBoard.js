import React from 'react'
import Cell from './Cell'

const GameBoard = ({gameState}) => {
    return (
        <div style = {boardStyle}>
            {gameState.map( cell =>  <Cell cellVal = {cell.cellValue} /> )}
        </div>
    )
}

const boardStyle = {
    width: "18rem",
    display: 'grid',
    gridTemplateColumns: "1fr 1fr 1fr 1fr 1fr 1fr 1fr 1fr 1fr",
}

export default GameBoard
