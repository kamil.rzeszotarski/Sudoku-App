import React, {useState, useEffect} from 'react';

import GameBoard from './components/GameBoard'

function App() {
  const [cellValues, setCellValues] = useState([])

  // Populate cellValues state with mock data
  useEffect(()=>{
    const gameBoardTemp = []
    for(let i = 0; i < 81; i++){
       gameBoardTemp.push({cellID: i, cellValue: i})
    }
    setCellValues(gameBoardTemp)
  },[])

  return (
    <div className="App">
      <h1>Sudoku App</h1>
      <GameBoard gameState={cellValues}/>
    </div>
  );
}

export default App;
